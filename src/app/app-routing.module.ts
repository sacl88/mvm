import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WebServiceComponent } from './web-service/web-service.component';

const appRoutes: Routes = [
    { path: '', component: WebServiceComponent },
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(appRoutes)]
})

export class AppRoutingModule {
}