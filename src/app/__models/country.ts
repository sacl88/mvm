export class Country {
    constructor(
        name: string,
        topLevelDomain: Array<any>,
        alpha2Code: string,
        alpha3Code: string,
        callingCodes: Array<any>,
        capital: string,
        altSpellings: Array<any>,
        region: string,
        subregion: string,
        population: number,
        latlng: Array<any>,
        demonym: string,
        area: number,
        gini: number,
        timezones: Array<any>,
        borders: Array<any>,
        nativeName: string,
        numericCode:(string | number),
        currencies:Array<any>,
        languages:Array<any>,
        translations:object,
        flag:string,
        regionalBlocs:Array<any>,
        cioc: string
    ) {


    }
}