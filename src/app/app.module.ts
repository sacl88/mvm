import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { WebServiceComponent } from './web-service/web-service.component';
import { AppRoutingModule } from './app-routing.module';
import { CharacterString } from './libs/character-string'
@NgModule({
  declarations: [
    AppComponent,
    WebServiceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    CharacterString
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
