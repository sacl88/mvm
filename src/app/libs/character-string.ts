export class CharacterString {
    private text;
    private clones;
    private totalArrayClones;
    private generation;
    private iterations;
    private characters;

    constructor() {
        this.clones = [];
        this.totalArrayClones = [];
        this.iterations = 50;
        this.generation = [];
    }

    getCharacterString() {
        this.text = "MVM INGENIERIA DE SOFTWARE";
        let generation = this.generation;
        this.characters = this.generateClones(this.text);
        let pointMax = this.getPointMax(this.characters);
        this.generation.push(pointMax);
        if (pointMax.point != 26) {
        } else {
            return this.generation
        }
        return generation;
    }

    getPointMax(result) {
        let obj_point_max = undefined;
        for (let i = 0; i < result.length; i++) {
            if (typeof obj_point_max == "undefined") {
                obj_point_max = result[i];
            } else {
                if (result[i].point > obj_point_max.point) {
                    obj_point_max = result[i];
                }
            }
        } 
        return obj_point_max;            
    }

    newCharacterAleatory(number, string) {
        string = Array.from(string)
        let aleatory_characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
       
        let newCharacter = '';
        for (let i = 0; i < string.length; i++) {
            let numberRandom = Math.random() * (number - 1) + 1;
            if (numberRandom == (i + 1)) {
                let resultsNewCharacter = aleatory_characters.charAt(Math.floor(Math.random() * aleatory_characters.length));
                console.log();
                newCharacter = newCharacter.concat(resultsNewCharacter);
            }else{
                newCharacter = newCharacter.concat(string[i]);
            }
        }
        return newCharacter;
    }

    generateClones(text) {
        for(let i = 0; i < this.iterations; i++) {
            let character = this.newCharacterAleatory(text.length, text);
            let objCharacter = {
                character: character,
                point: this.assignPointCharacter(character)
            };
            this.totalArrayClones.push(objCharacter);
            if(i == (this.iterations - 1)) {
                return this.totalArrayClones;    
            }
        }    
    }    

    assignPointCharacter(character) {
        let point = 0;
        character = Array.from(character)
        let text = Array.from(this.text);
        
        for(let i = 0; i < character.length; i ++) {
            if(text[i] == character[i]) {
                point++;
            }
        }
        
        return point;
    }
}