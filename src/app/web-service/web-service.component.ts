import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CountryService } from '../__services/country.service';
import { Country } from '../__models/country';
import { HttpErrorResponse } from '@angular/common/http';
import { CharacterString } from '../libs/character-string'
@Component({
  selector: 'app-web-service',
  templateUrl: './web-service.component.html',
  styleUrls: ['./web-service.component.css'],
})
export class WebServiceComponent implements OnInit {

  f: FormGroup;
  country: Country;
  isLoadingResults = false;
  error: any = [];
  keysErrors = [];
  displayErrors:boolean = false;
  message: any;
  constructor(
    private formBuilder: FormBuilder,
    private countryService: CountryService,
  ) {
  }

  ngOnInit() {
    this.f = this.formBuilder.group({
      iso: [null, [
            Validators.required,
            Validators.minLength(3), 
            Validators.pattern('^[a-zA-Z0-9]+$'),
            Validators.maxLength(3)
          ]
        ]
    })
    let classCharacter = new CharacterString();
    let characteres = classCharacter.getCharacterString();
    for(let i = 0; i < characteres.length; i++) {      
      console.log("Generación: " + i + " - " + characteres[i].character + " - " + characteres[i].point);
    }
  }

  infoCountry() {
    this.isLoadingResults = true;
    this.displayErrors = false;
    this.countryService.infoCountry(this.f.value.iso).subscribe(
      (resp) => {
        this.isLoadingResults = false;
        this.country = resp;
      },
      (errorResponse: HttpErrorResponse) => {
        this.displayErrors = true;
        this.error = "No se encontraron resultados";
      }
    )
  }

}
